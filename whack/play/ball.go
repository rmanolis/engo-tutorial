package play

import (
	"image/color"
	"math/rand"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
	"github.com/coderconvoy/engotil"
)

type Ball struct {
	ecs.BasicEntity
	DragComponent
	engotil.VelocityComponent
	common.RenderComponent
	common.SpaceComponent
	engotil.GCollisionComponent
}

func NewBall(x, y, w float32) *Ball {
	res := &Ball{
		BasicEntity:       ecs.NewBasic(),
		VelocityComponent: engotil.VelocityComponent{Friction: 0.5},
		RenderComponent: common.RenderComponent{
			Drawable: common.Circle{},
			Color:    color.Black,
		},
		DragComponent: DragComponent{w},
		SpaceComponent: common.SpaceComponent{
			Position: engo.Point{x, y},
			Width:    w,
			Height:   w,
		},
		GCollisionComponent: engotil.GCollisionComponent{
			Extra: engo.Point{0, 0},
			Main:  C_BALL_HIT | C_MOVING_SOLID,
			Group: 0,
		},
	}
	res.SetZIndex(5)
	return res
}

func AddBall(partner Draggable, fric, l float32, sl SysList) *Ball {
	psc := partner.GetSpaceComponent().Position

	c := NewBall(psc.X+rand.Float32()*l/2, psc.Y+rand.Float32()*l/2, 10)
	sl.DragSys.Connect(partner, c, fric, l)
	sl.RenderSys.AddByInterface(c)
	sl.VelSys.Add(c)
	sl.CollisionSys.Add(c)
	return c
}
