package play

import (
	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
	"github.com/coderconvoy/engotil"
)

const (
	C_BOY_HURT = 1 << iota
	C_BOY_SOLID
	C_BALL_HIT
	C_MOVING_SOLID
)

type Boy struct {
	ecs.BasicEntity
	engotil.VelocityComponent
	common.RenderComponent
	common.SpaceComponent
	engotil.GCollisionComponent
	DragComponent
	ControlComponent
	ss *common.Spritesheet
}

func NewBoy(x, y, w float32) *Boy {
	ss := common.NewSpritesheetFromFile("rpgcritters2.png", 55, 50)
	res := &Boy{
		BasicEntity:       ecs.NewBasic(),
		VelocityComponent: engotil.VelocityComponent{Friction: 10},
		DragComponent:     DragComponent{w},
		RenderComponent: common.RenderComponent{
			Drawable: ss.Cell(4),
		},
		SpaceComponent: common.SpaceComponent{
			Position: engo.Point{x, y},
			Width:    w,
			Height:   w,
		},
		GCollisionComponent: engotil.GCollisionComponent{
			Extra: engo.Point{-10, -10},
			Main:  C_BOY_SOLID | C_MOVING_SOLID | C_BOY_HURT,
			Group: C_MOVING_SOLID,
		},
		ControlComponent: GetKeys(),
		ss:               ss,
	}
	res.SetZIndex(5)
	return res
}
