package play

import (
	"engo.io/ecs"
	"engo.io/engo"
	"github.com/coderconvoy/engotil"
	"github.com/prometheus/common/log"
)

type BoxSystem struct {
	rats    []*Rat
	targets []engotil.Spaceable
}

func (bs *BoxSystem) AddTarget(t engotil.Spaceable) {
	bs.targets = append(bs.targets, t)
}

func (bs *BoxSystem) AddBox(b *Rat) {
	bs.rats = append(bs.rats, b)
}

func (bs *BoxSystem) Update(d float32) {
	if len(bs.targets) == 0 {
		return
	}

	for _, b := range bs.rats {
		//loop for nearest target
		bcen := engotil.SpaceCenter(*b.GetSpaceComponent())
		nearest := bs.targets[0]
		ncen := engotil.SpaceCenter(*nearest.GetSpaceComponent())
		nd2 := bcen.PointDistanceSquared(ncen)

		for _, t := range bs.targets {
			ncen = engotil.SpaceCenter(*t.GetSpaceComponent())
			td2 := bcen.PointDistanceSquared(ncen)
			if td2 < nd2 {
				nearest = t
				nd2 = td2
			}
		}

		//push towards nearest

		ncen = engotil.SpaceCenter(*nearest.GetSpaceComponent())
		if bcen.X > ncen.X {
			b.Push(engo.Point{-b.acc, 0})
		}
		if bcen.X < ncen.X {
			b.Push(engo.Point{b.acc, 0})
		}
		if bcen.Y > ncen.Y {
			b.Push(engo.Point{0, -b.acc})
		}
		if bcen.Y < ncen.Y {
			b.Push(engo.Point{0, b.acc})
		}

	}
}

func (bs *BoxSystem) Remove(e ecs.BasicEntity) {
	bs.targets = engotil.RemoveSpaceable(bs.targets, e)
	dp := -1
	for i, v := range bs.rats {
		if v.ID() == e.ID() {
			dp = i
			log.Info("deletes ", i)
			break
		}
	}
	if dp >= 0 {
		bs.rats = append(bs.rats[:dp], bs.rats[dp+1:]...)
	}
}
