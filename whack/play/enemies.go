package play

import (
	"fmt"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
	"github.com/coderconvoy/engotil"
)

type Rat struct {
	ecs.BasicEntity
	common.SpaceComponent
	common.RenderComponent
	ss *common.Spritesheet
	engotil.VelocityComponent
	engotil.GCollisionComponent
	acc float32
}

func NewRat(cell_num int, lev int, x, y float32, sheet *common.Spritesheet) *Rat {
	return &Rat{
		BasicEntity: ecs.NewBasic(),
		SpaceComponent: common.SpaceComponent{
			Position: engo.Point{x, y},
			Width:    40,
			Height:   40,
		},
		RenderComponent: common.RenderComponent{
			Drawable: sheet.Cell(cell_num),
		},
		VelocityComponent: engotil.VelocityComponent{
			Friction: 2,
		},
		GCollisionComponent: engotil.GCollisionComponent{
			Main:  C_MOVING_SOLID,
			Group: C_BOY_HURT | C_BALL_HIT,
			Extra: engo.Point{-10, -10},
		},
		acc: float32(lev+3) * 0.05,
		ss:  sheet,
	}
}

type HitSystem struct {
	NPlayers int
}

func (hs *HitSystem) New(w *ecs.World) {
	engo.Mailbox.Listen("GCollisionMessage", func(m engo.Message) {
		cm, ok := m.(engotil.GCollisionMessage)
		if !ok {
			fmt.Println("Could not Convert Message")
			return
		}
		_, isBall := cm.Main.(*Ball)
		_, isBoy := cm.Main.(*Boy)

		_, isBox := cm.Buddy.(*Rat)
		if isBoy && isBox {
			fmt.Println("Killing")
		}

		if isBall && isBox {
			fmt.Println("Removing")
			w.RemoveEntity(*cm.Buddy.GetBasicEntity())

		}

	})

}
func (hs *HitSystem) Update(d float32) {}
func (hs *HitSystem) Remove(e ecs.BasicEntity) {
}
