package play

import (
	"image/color"
	"log"

	"engo.io/ecs"
	"engo.io/engo/common"
	"github.com/coderconvoy/engotil"

	"engo.io/engo"
)

type SysList struct {
	RenderSys    *common.RenderSystem
	BoxSys       *BoxSystem
	DragSys      *DragSystem
	ControlSys   *ControlSystem
	CollisionSys *engotil.GCollisionSystem
	VelSys       *engotil.VelocitySystem
}

type MainScene struct{ NPlayers int }

func (*MainScene) Type() string { return "MainScene" }

func (*MainScene) Preload() {
	err := engo.Files.Load("Targa.ttf")
	if err != nil {
		log.Fatal(err)
	}
	err = engo.Files.Load("rpgcritters2.png")
	if err != nil {
		log.Fatal(err)
	}
}

func (*MainScene) Setup(w *ecs.World) {
	common.SetBackground(color.White)
	fnt := &common.Font{
		URL:  "Targa.ttf",
		FG:   color.Black,
		Size: 36,
	}

	err := fnt.CreatePreloaded()
	if err != nil {
		log.Fatal(err)
	}

	sys := SysList{}
	sys.RenderSys = &common.RenderSystem{}
	sys.DragSys = &DragSystem{}
	sys.BoxSys = &BoxSystem{}
	sys.VelSys = &engotil.VelocitySystem{}
	sys.ControlSys = &ControlSystem{}
	sys.CollisionSys = &engotil.GCollisionSystem{Solids: C_BOY_SOLID | C_MOVING_SOLID}

	var sx float32 = 100.0
	var sy float32 = 100.0

	a := NewBoy(sx, sy, 20)
	sys.RenderSys.AddByInterface(a)
	sys.VelSys.Add(a)
	for _, kc := range a.GetControls() {
		engo.Input.RegisterButton(kc.S, kc.K)
	}
	sys.ControlSys.Add(a)
	sys.CollisionSys.Add(a)
	sys.BoxSys.AddTarget(a)
	AddBall(a, 0.1, 70, sys)

	w.AddSystem(sys.RenderSys)
	w.AddSystem(sys.BoxSys)
	w.AddSystem(sys.DragSys)
	w.AddSystem(sys.VelSys)
	w.AddSystem(sys.ControlSys)
	w.AddSystem(sys.CollisionSys)
	w.AddSystem(&HitSystem{NPlayers: 1})
	w.AddSystem(NewSpawnSystem(sys))
}
