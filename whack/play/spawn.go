package play

import (
	"math/rand"

	"engo.io/ecs"
	"engo.io/engo/common"
)

type SpawnSystem struct {
	S     SysList
	Delay float32
	since float32
	rat   *common.Spritesheet
}

func NewSpawnSystem(sl SysList) *SpawnSystem {
	return &SpawnSystem{
		S:     sl,
		Delay: 3,
		rat:   common.NewSpritesheetFromFile("rpgcritters2.png", 55, 50),
	}
}

func (ss *SpawnSystem) Update(d float32) {
	ss.since += d
	n := rand.Intn(2)
	if ss.since > ss.Delay {
		r := NewRat(n, 1, float32(100*n), float32(100*n), ss.rat)
		ss.S.RenderSys.Add(&r.BasicEntity, &r.RenderComponent, &r.SpaceComponent)
		ss.S.BoxSys.AddBox(r)
		ss.S.VelSys.Add(r)
		ss.S.CollisionSys.Add(r)
		ss.since = 0
		ss.Delay -= 0.01
	}
}

func (ss *SpawnSystem) Remove(e ecs.BasicEntity) {

}
