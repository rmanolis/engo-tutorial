package main

import (
	"flag"
	"log"

	"engo-tutorial/whack/play"

	"engo.io/engo"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	np := flag.Int("np", 1, "np: Number of players")
	flag.Parse()
	opts := engo.RunOptions{
		Title:         "Whack",
		ScaleOnResize: true,
		Width:         700,
		Height:        700,
	}
	engo.Run(opts, &play.MainScene{*np})
}
