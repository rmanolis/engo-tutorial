package main

import (
	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
	"golang.org/x/image/colornames"
)

type Human struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
	common.MouseComponent
	common.CollisionComponent
	HealthComponent
	SelectComponent
	ControlComponent
	SpeedComponent
}

const (
	PERSON = 1 << iota
)

func NewHuman(point engo.Point) *Human {
	human := Human{BasicEntity: ecs.NewBasic()}
	human.SpaceComponent = common.SpaceComponent{Position: point, Width: 50, Height: 100}
	blue := colornames.Blue
	human.RenderComponent = common.RenderComponent{Drawable: common.Rectangle{}, Color: blue}
	human.CollisionComponent = common.CollisionComponent{
		Main:  PERSON,
		Group: PERSON,
	}
	human.ControlComponent = ControlComponent{
		SchemeHoriz: "horizontal",
		SchemeVert:  "vertical",
	}
	return &human
}
