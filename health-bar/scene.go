package main

import (
	"image/color"

	"engo.io/engo"

	"engo.io/ecs"

	"engo.io/engo/common"
)

type GlobalSystems struct {
	*common.RenderSystem
	*common.MouseSystem
	*common.CollisionSystem
	*ecs.World
}

type Scene struct{}

func (*Scene) Preload() {
	engo.Input.RegisterButton(upButton, engo.W, engo.ArrowUp)
	engo.Input.RegisterButton(leftButton, engo.A, engo.ArrowLeft)
	engo.Input.RegisterButton(rightButton, engo.D, engo.ArrowRight)
	engo.Input.RegisterButton(downButton, engo.S, engo.ArrowDown)
}
func (*Scene) Type() string { return "Health-Bar-Game" }

func (*Scene) Setup(w *ecs.World) {
	common.SetBackground(color.White)

	gs := &GlobalSystems{}
	gs.World = w
	gs.RenderSystem = &common.RenderSystem{}
	gs.MouseSystem = &common.MouseSystem{}
	gs.CollisionSystem = &common.CollisionSystem{Solids: PERSON}
	hs := NewHealthSystem(gs)
	ss := NewSelectSystem(gs)
	sps := NewSpeedSystem()
	ct := NewControlSystem()
	for i := 0; i < 5; i++ {
		human := NewHuman(engo.Point{float32(100 * i), 100})
		gs.RenderSystem.AddByInterface(human)
		gs.MouseSystem.AddByInterface(human)
		gs.CollisionSystem.AddByInterface(human)
		human.Health = 50
		human.Selected = false
		hs.Add(human)
		ss.AddOne(human)
		ct.Add(human)
		sps.Add(human)
	}
	w.AddSystem(gs.RenderSystem)
	w.AddSystem(gs.MouseSystem)
	w.AddSystem(gs.CollisionSystem)
	w.AddSystem(hs)
	w.AddSystem(ss)
	w.AddSystem(ct)
	w.AddSystem(sps)
}
