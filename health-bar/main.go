package main

import (
	"engo.io/engo"
)

const (
	WIDTH  = 800
	HEIGHT = 600
)

func main() {
	opts := engo.RunOptions{
		Title:          "Demo health bar game",
		Width:          WIDTH,
		Height:         HEIGHT,
		StandardInputs: true,
		MSAA:           4,
	}
	engo.Run(opts, &Scene{})
}
