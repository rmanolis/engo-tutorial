package main

import (
	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
)

type ControlComponent struct {
	SchemeVert  string
	SchemeHoriz string
}

func (s *ControlComponent) GetControlComponent() *ControlComponent {
	return s
}

type ControlFace interface {
	GetControlComponent() *ControlComponent
}

type Controlable interface {
	ecs.Identifier
	common.SpaceFace
	ControlFace
	SelectFace
}

type ControlSystem struct {
	entities []Controlable
}

func NewControlSystem() *ControlSystem {
	cs := ControlSystem{}
	return &cs
}

func (c *ControlSystem) Add(e Controlable) {
	c.entities = append(c.entities, e)
}

func (c *ControlSystem) Remove(basic ecs.BasicEntity) {
	delete := -1
	for index, e := range c.entities {
		if e.ID() == basic.ID() {
			delete = index
			break
		}
	}
	if delete >= 0 {
		c.entities = append(c.entities[:delete], c.entities[delete+1:]...)
	}
}

func getSpeed(e Controlable) (p engo.Point, changed bool) {
	p.X = engo.Input.Axis(e.GetControlComponent().SchemeHoriz).Value()
	p.Y = engo.Input.Axis(e.GetControlComponent().SchemeVert).Value()
	origX, origY := p.X, p.Y

	if engo.Input.Button(upButton).JustPressed() {
		p.Y = -1
	} else if engo.Input.Button(downButton).JustPressed() {
		p.Y = 1
	}
	if engo.Input.Button(leftButton).JustPressed() {
		p.X = -1
	} else if engo.Input.Button(rightButton).JustPressed() {
		p.X = 1
	}

	if engo.Input.Button(upButton).JustReleased() || engo.Input.Button(downButton).JustReleased() {
		p.Y = 0
		changed = true
	}
	if engo.Input.Button(leftButton).JustReleased() || engo.Input.Button(rightButton).JustReleased() {
		p.X = 0
		changed = true
	}
	changed = changed || p.X != origX || p.Y != origY
	return
}

func (c *ControlSystem) Update(dt float32) {
	for _, e := range c.entities {
		if e.GetSelectComponent().Selected {
			if vector, changed := getSpeed(e); changed {
				speed := dt * SPEED_SCALE
				vector.MultiplyScalar(speed)
				engo.Mailbox.Dispatch(SpeedMessage{e, vector})
			}
		}

	}
}
