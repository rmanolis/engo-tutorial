package main

import (
	"log"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
)

const (
	SPEED_SCALE   = 64
	SPEED_MESSAGE = "SpeedMessage"
)

type SpeedMessage struct {
	ecs.Identifier
	engo.Point
}

func (SpeedMessage) Type() string {
	return SPEED_MESSAGE
}

type SpeedComponent struct {
	engo.Point
}

func (s *SpeedComponent) GetSpeedComponent() *SpeedComponent {
	return s
}

type SpeedFace interface {
	GetSpeedComponent() *SpeedComponent
}

type Speedable interface {
	ecs.Identifier
	common.SpaceFace
	SpeedFace
	SelectFace
}

type SpeedSystem struct {
	entities []Speedable
}

func NewSpeedSystem() *SpeedSystem {
	ss := SpeedSystem{}
	engo.Mailbox.Listen(SPEED_MESSAGE, func(message engo.Message) {
		speed, isSpeed := message.(SpeedMessage)
		if isSpeed {
			log.Printf("%#v\n", speed.Point)
			for _, e := range ss.entities {
				if e.ID() == speed.ID() {
					e.GetSpeedComponent().Point = speed.Point
				}
			}
		}
	})
	engo.Mailbox.Listen("CollisionMessage", func(message engo.Message) {
		collision, isCollision := message.(common.CollisionMessage)
		if isCollision {
			for _, e := range ss.entities {
				if e.ID() == collision.Entity.BasicEntity.ID() {
					e.GetSpeedComponent().X *= -1
					e.GetSpeedComponent().Y *= -1
				}
			}
			/*for _, e := range ss.entities {
				if e.ID() == collision.To.BasicEntity.ID() {
					e.GetSpeedComponent().X *= +1
					e.GetSpeedComponent().Y *= +1
				}
			}*/
		}
	})
	return &ss
}

func (s *SpeedSystem) Add(sp Speedable) {
	s.entities = append(s.entities, sp)
}

func (s *SpeedSystem) Remove(basic ecs.BasicEntity) {
	delete := -1
	for index, e := range s.entities {
		if e.ID() == basic.ID() {
			delete = index
			break
		}
	}
	if delete >= 0 {
		s.entities = append(s.entities[:delete], s.entities[delete+1:]...)
	}
}

func (s *SpeedSystem) Update(dt float32) {

	for _, e := range s.entities {
		if e.GetSelectComponent().Selected {
			speed := engo.GameWidth() * dt
			setSpeed(e, speed)
		}
	}

}

func setSpeed(e Speedable, speed float32) {
	e.GetSpaceComponent().Position.X = e.GetSpaceComponent().Position.X + speed*e.GetSpeedComponent().Point.X
	e.GetSpaceComponent().Position.Y = e.GetSpaceComponent().Position.Y + speed*e.GetSpeedComponent().Point.Y

	// Add Game Border Limits
	var heightLimit float32 = HEIGHT - e.GetSpaceComponent().Height
	if e.GetSpaceComponent().Position.Y < 0 {
		e.GetSpaceComponent().Position.Y = 0
	} else if e.GetSpaceComponent().Position.Y > heightLimit {
		e.GetSpaceComponent().Position.Y = heightLimit
	}

	var widthLimit float32 = WIDTH - e.GetSpaceComponent().Width
	if e.GetSpaceComponent().Position.X < 0 {
		e.GetSpaceComponent().Position.X = 0
	} else if e.GetSpaceComponent().Position.X > widthLimit {
		e.GetSpaceComponent().Position.X = widthLimit
	}
}
