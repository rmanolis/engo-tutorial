package main

import (
	"fmt"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
	"golang.org/x/image/colornames"
)

var (
	upButton    = "up"
	downButton  = "down"
	leftButton  = "left"
	rightButton = "right"
)

type SelectComponent struct {
	Selected bool
}

func (s *SelectComponent) GetSelectComponent() *SelectComponent {
	return s
}

type SelectFace interface {
	GetSelectComponent() *SelectComponent
}

type Selectable interface {
	ecs.Identifier
	common.RenderFace
	common.SpaceFace
	common.MouseFace
	SelectFace
}

type Pointer struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
}

func NewPointer(point_obj engo.Point) *Pointer {
	hb := Pointer{BasicEntity: ecs.NewBasic()}
	point_obj.Add(engo.Point{20, -40})
	hb.SpaceComponent = common.SpaceComponent{Position: point_obj, Width: 5, Height: 50, Rotation: 180}
	green := colornames.Green
	hb.RenderComponent = common.RenderComponent{Drawable: common.Triangle{}, Color: green}
	return &hb
}

func (p *Pointer) OverObject(point_obj engo.Point) {
	point_obj.Add(engo.Point{20, -40})
	p.Position = point_obj
}

type BeforeAfterPoint struct {
	Before engo.Point
	After  engo.Point
}

type SelectedEntity struct {
	Entity  Selectable
	Pointer *Pointer
}

type SelectSystem struct {
	gs                *GlobalSystems
	entities          map[uint64]SelectedEntity
	beforeAfterPoint  *BeforeAfterPoint
	mouseTrackerBasic ecs.BasicEntity
	mouseTrackerMouse common.MouseComponent
}

func NewSelectSystem(g *GlobalSystems) *SelectSystem {
	ss := &SelectSystem{
		gs:       g,
		entities: map[uint64]SelectedEntity{},
	}
	ss.mouseTrackerBasic = ecs.NewBasic()
	ss.mouseTrackerMouse.Track = true
	for _, system := range ss.gs.World.Systems() {
		switch sys := system.(type) {
		case *common.MouseSystem:
			sys.Add(&ss.mouseTrackerBasic, &ss.mouseTrackerMouse, nil, nil)
		}
	}

	engo.Mailbox.Listen("CollisionMessage", func(message engo.Message) {
		collision, isCollision := message.(common.CollisionMessage)
		if isCollision {

			for _, e := range ss.entities {
				if e.Entity.ID() == collision.Entity.BasicEntity.ID() && e.Entity.GetSelectComponent().Selected {
					e.Entity.GetSpaceComponent().Position.X -= 1
					e.Entity.GetSpaceComponent().Position.Y -= 1
				}

			}
		}
	})
	return ss
}

func (h *SelectSystem) AddOne(e Selectable) {
	pointer := NewPointer(e.GetSpaceComponent().Position)
	h.entities[e.ID()] = SelectedEntity{
		Entity:  e,
		Pointer: pointer,
	}
}

func (h *SelectSystem) Remove(basic ecs.BasicEntity) {}

func containsPoints(bap *BeforeAfterPoint, scs []Selectable) []Selectable {
	res := []Selectable{}
	for _, v := range scs {
		pos := v.GetSpaceComponent().Position
		beforeX := bap.Before.X
		afterX := bap.After.X
		if bap.After.X < bap.Before.X {
			beforeX = bap.After.X
			afterX = bap.Before.X
		}
		if beforeX <= pos.X && pos.X <= afterX {
			res = append(res, v)
		}
	}
	res1 := []Selectable{}
	for _, v := range res {
		pos := v.GetSpaceComponent().Position
		beforeY := bap.Before.Y
		afterY := bap.After.Y
		if bap.After.Y < bap.Before.Y {
			beforeY = bap.After.Y
			afterY = bap.Before.Y
		}
		if beforeY <= pos.Y && pos.Y <= afterY {
			res1 = append(res1, v)
		}
	}
	return res1
}

func (h *SelectSystem) Update(dt float32) {
	if engo.Input.Mouse.Action == engo.Press && engo.Input.Mouse.Button == engo.MouseButtonLeft {
		h.beforeAfterPoint = new(BeforeAfterPoint)
		h.beforeAfterPoint.Before.X = engo.Input.Mouse.X
		h.beforeAfterPoint.Before.Y = engo.Input.Mouse.Y
	}
	if engo.Input.Mouse.Action == engo.Release && engo.Input.Mouse.Button == engo.MouseButtonLeft {
		fmt.Println("left button released")
		h.beforeAfterPoint.After.X = engo.Input.Mouse.X
		h.beforeAfterPoint.After.Y = engo.Input.Mouse.Y
		fmt.Println("the point ", h.beforeAfterPoint)
		sps := []Selectable{}
		for _, v := range h.entities {
			sps = append(sps, v.Entity)
		}
		sps = containsPoints(h.beforeAfterPoint, sps)
		if len(sps) > 0 {
			for _, v := range h.entities {
				v.Entity.GetSelectComponent().Selected = false
			}
			for _, v := range sps {
				h.entities[v.ID()].Entity.GetSelectComponent().Selected = true
			}
		}
		h.beforeAfterPoint = nil
	}
	for _, e := range h.entities {
		if e.Entity.GetMouseComponent().Clicked {
			for _, v := range h.entities {
				v.Entity.GetSelectComponent().Selected = false
			}
			e.Entity.GetSelectComponent().Selected = true
		}
	}

	for id, e := range h.entities {
		if e.Entity.GetSelectComponent().Selected {
			entity_sp := e.Entity.GetSpaceComponent()
			/*if engo.Input.Button(upButton).Down() {
				entity_sp.Position.Add(engo.Point{0, -4})
			} else if engo.Input.Button(downButton).Down() {
				entity_sp.Position.Add(engo.Point{0, 4})
			} else if engo.Input.Button(leftButton).Down() {
				entity_sp.Position.Add(engo.Point{-4, 0})
			} else if engo.Input.Button(rightButton).Down() {
				entity_sp.Position.Add(engo.Point{4, 0})
			}*/
			pos := entity_sp.Position
			pointer := h.entities[id].Pointer
			pointer.OverObject(pos)
			h.gs.RenderSystem.AddByInterface(pointer)
		} else {
			h.gs.RemoveEntity(e.Pointer.BasicEntity)
		}
	}
}
