package main

import (
	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
	"golang.org/x/image/colornames"
)

type HealthComponent struct {
	Health int
}

func (h *HealthComponent) GetHealthComponent() *HealthComponent {
	return h
}

type HealthFace interface {
	GetHealthComponent() *HealthComponent
}

type Healthable interface {
	ecs.Identifier
	common.RenderFace
	common.SpaceFace
	HealthFace
}

type HealthEntity struct {
	Entity Healthable
	Bar    *LifeBar
}

type HealthSystem struct {
	gs       *GlobalSystems
	entities map[uint64]HealthEntity
}

func NewHealthSystem(g *GlobalSystems) *HealthSystem {
	return &HealthSystem{
		gs:       g,
		entities: map[uint64]HealthEntity{},
	}
}

func (h *HealthSystem) Add(ha Healthable) {
	pos := ha.GetSpaceComponent().Position
	hb := NewLifeBar(pos)
	h.gs.RenderSystem.AddByInterface(hb)
	h.entities[ha.ID()] = HealthEntity{
		Entity: ha,
		Bar:    hb,
	}

}

func (h *HealthSystem) Remove(basic ecs.BasicEntity) {

}

func (h *HealthSystem) Update(dt float32) {
	for id, e := range h.entities {
		pos := e.Entity.GetSpaceComponent().Position
		bar := h.entities[id].Bar
		bar.OverObject(pos)
		bar.UpdateHealth(e.Entity.GetHealthComponent().Health)
	}
}

type LifeBar struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
}

func NewLifeBar(point_obj engo.Point) *LifeBar {
	point_obj.Add(engo.Point{0, -10})
	hb := LifeBar{BasicEntity: ecs.NewBasic()}
	hb.SpaceComponent = common.SpaceComponent{Position: point_obj, Width: 50, Height: 5}
	red := colornames.Red
	hb.RenderComponent = common.RenderComponent{Drawable: common.Rectangle{}, Color: red}
	return &hb
}

func (lb *LifeBar) OverObject(point_obj engo.Point) {
	point_obj.Add(engo.Point{0, -10})
	lb.Position = point_obj
}
func (lb *LifeBar) UpdateHealth(health int) {
	lb.Width = float32(50 * health / 100)
}
