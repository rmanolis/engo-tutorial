
First we created the scene. 
For the scene we set background color and we submitted it to the main function.
Then we created the Frog entity, and submitted in which point should start.
From there we added the frog to screen by adding to a global RenderSystem and that submitted to the world.

After we created a Car entity with the same components as Frog.
However we created a new car on different position based on the update.
The car creation happened from SpawnSystem.
The SpawnSystem on each update it was creating a new car.
We submitted the SpawnSystem to the world.

We created the VelocityComponent and added to the Car.
Then we created the MovementSystem, where we add the Position and the Velocity of the car.
Then the MovementSystem on each update, it updates the Position of each car based on the Velocity.

Next created a BoundSystem, where we add the Car entities and check if its position is over 600 or less than 0.
If it is , it calls the world to remove this entity from ALL the systems and eventually from the display also.

After creating the BoundSystem, we created the ControlSystem for the frog.
But first we created the ControlComponent with a list of keys that will control a frog.
Then we created the ControlEntity, so we can create a nice list of entities in the ControlSystem, 
that we will change their position when the represented key is pressed.