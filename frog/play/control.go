package play

import (
	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
)

type KeyCommand struct {
	K engo.Key
	S string
	P engo.Point
}

type ControlComponent struct {
	commands []KeyCommand
}

func (cc *ControlComponent) GetControlComponent() *ControlComponent {
	return cc
}

func (cc *ControlComponent) GetControls() []KeyCommand {
	return cc.commands
}

func NewControls(n int) ControlComponent {
	if n == 0 {
		return ControlComponent{
			commands: []KeyCommand{
				{engo.ArrowLeft, "l1", engo.Point{-50, 0}},
				{engo.ArrowRight, "r1", engo.Point{50, 0}},
				{engo.ArrowUp, "u1", engo.Point{0, -50}},
				{engo.ArrowDown, "d1", engo.Point{0, 50}},
			},
		}
	}

	return ControlComponent{
		commands: []KeyCommand{
			{engo.A, "l2", engo.Point{-50, 0}},
			{engo.D, "r2", engo.Point{50, 0}},
			{engo.W, "u2", engo.Point{0, -50}},
			{engo.S, "d2", engo.Point{0, 50}},
		},
	}
}

type ControlEntity struct {
	*ecs.BasicEntity
	*common.SpaceComponent
	*ControlComponent
}

type ControlSystem struct {
	obs []ControlEntity
}

func (cs *ControlSystem) Add(be *ecs.BasicEntity, sc *common.SpaceComponent, cc *ControlComponent) {
	cs.obs = append(cs.obs, ControlEntity{be, sc, cc})
}

func (cs *ControlSystem) Update(d float32) {
	for _, c := range cs.obs {
		for _, kc := range c.GetControls() {
			if engo.Input.Button(kc.S).JustPressed() {
				sc := c.GetSpaceComponent()
				sc.Position.Add(kc.P)
			}
		}
	}
}

func (cs *ControlSystem) Remove(be ecs.BasicEntity) {
	dp := -1
	for i, v := range cs.obs {
		if v.ID() == be.ID() {
			dp = i
			break
		}
	}
	if dp >= 0 {
		cs.obs = append(cs.obs[:dp], cs.obs[dp+1:]...)
	}
}
