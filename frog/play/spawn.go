package play

import (
	"math/rand"

	"engo.io/engo"

	"engo.io/ecs"
	"engo.io/engo/common"
)

type SysList struct {
	*common.RenderSystem
	*MovementSystem
	*BoundsSystem
}
type SpawnSystem struct {
	since   float32
	nCars   int
	Systems *SysList
}

func (ss *SpawnSystem) Remove(c ecs.BasicEntity) {

}

func (ss *SpawnSystem) Update(d float32) {
	ss.since = ss.since + d
	if ss.since > 1 {
		row := rand.Intn(7)
		vel := engo.Point{float32((row%2)*60 - 20), 0}
		pos := engo.Point{float32((row%2)*-800 + 700), float32(row * 50)}
		ss.nCars += 1
		ss.since = 0
		c := NewCar(pos, vel)
		ss.Systems.RenderSystem.AddByInterface(c)
		ss.Systems.MovementSystem.Add(&c.BasicEntity, &c.SpaceComponent, &c.VelocityComponent)
		ss.Systems.BoundsSystem.Add(&c.BasicEntity, &c.SpaceComponent, &c.VelocityComponent)
	}
}
