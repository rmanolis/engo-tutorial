package play

import (
	"log"

	"engo.io/ecs"
	"engo.io/engo/common"
)

type BoundsSystem struct {
	W   *ecs.World
	obs []MovementEntity
}

func (bs *BoundsSystem) Add(be *ecs.BasicEntity, sp *common.SpaceComponent, vl *VelocityComponent) {
	bs.obs = append(bs.obs, MovementEntity{be, sp, vl})
}

func (bs *BoundsSystem) Update(d float32) {
	t := bs.obs
	for _, c := range t {
		sc := c.SpaceComponent
		vc := c.VelocityComponent.Vel
		if sc.Position.X+sc.Width < 0 && vc.X < 0 {
			bs.W.RemoveEntity(*c.BasicEntity)
		}
		if sc.Position.X > 600 && vc.X > 0 {
			bs.W.RemoveEntity(*c.BasicEntity)
		}
	}
}

func (bs *BoundsSystem) Remove(be ecs.BasicEntity) {
	log.Println("calls remove from bounds system")
	dp := -1
	for i, v := range bs.obs {
		if v.ID() == be.ID() {
			dp = i
			log.Println("deletes ", i)
			break
		}
	}
	if dp >= 0 {
		bs.obs = append(bs.obs[:dp], bs.obs[dp+1:]...)
	}
}
