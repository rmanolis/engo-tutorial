package play

import (
	"image/color"

	"engo.io/engo"

	"engo.io/ecs"
	"engo.io/engo/common"
)

type PlayScene struct{}

func (*PlayScene) Type() string { return "PlayScene" }

func (*PlayScene) Preload() {}

func (*PlayScene) Setup(w *ecs.World) {
	common.SetBackground(color.White)
	sysList := &SysList{
		RenderSystem:   &common.RenderSystem{},
		MovementSystem: &MovementSystem{},
		BoundsSystem:   &BoundsSystem{W: w},
	}

	spanSys := &SpawnSystem{
		Systems: sysList,
	}
	controlSys := &ControlSystem{}
	fg := NewFrog(0)
	for _, kc := range fg.GetControls() {
		engo.Input.RegisterButton(kc.S, kc.K)
	}
	sysList.RenderSystem.AddByInterface(fg)
	controlSys.Add(&fg.BasicEntity, &fg.SpaceComponent, &fg.ControlComponent)

	w.AddSystem(controlSys)
	w.AddSystem(sysList.RenderSystem)
	w.AddSystem(sysList.MovementSystem)
	w.AddSystem(sysList.BoundsSystem)
	w.AddSystem(spanSys)

}
