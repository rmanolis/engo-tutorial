package play

import (
	"image/color"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
	"github.com/prometheus/common/log"
)

type Car struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
	VelocityComponent
}

func NewCar(pos, vel engo.Point) *Car {
	return &Car{
		BasicEntity: ecs.NewBasic(),
		SpaceComponent: common.SpaceComponent{
			Position: pos,
			Width:    100,
			Height:   50,
		},
		RenderComponent: common.RenderComponent{
			Drawable: common.Rectangle{},
			Color:    color.Black,
		},
		VelocityComponent: VelocityComponent{vel},
	}
}

type MovementEntity struct {
	*ecs.BasicEntity
	*common.SpaceComponent
	*VelocityComponent
}

type MovementSystem struct {
	obs []MovementEntity
}

func (ms *MovementSystem) Add(be *ecs.BasicEntity, sc *common.SpaceComponent, vc *VelocityComponent) {
	ms.obs = append(ms.obs, MovementEntity{
		be, sc, vc,
	})
}

func (ms *MovementSystem) Remove(e ecs.BasicEntity) {
	log.Info("calls remove for movement system")
	dp := -1
	for i, v := range ms.obs {
		if v.ID() == e.ID() {
			dp = i
			log.Info("deletes ", i)
			break
		}
	}
	if dp >= 0 {
		ms.obs = append(ms.obs[:dp], ms.obs[dp+1:]...)
	}

}

func (ms *MovementSystem) Update(d float32) {
	for _, c := range ms.obs {
		c.Position.X += c.Vel.X * d
		c.Position.Y += c.Vel.Y * d
	}
}
