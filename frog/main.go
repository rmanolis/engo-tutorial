package main

import (
	"engo-tutorial/frog/play"

	"engo.io/engo"
)

func main() {
	opts := engo.RunOptions{
		Width:  700,
		Height: 500,
		Title:  "The frog",
	}
	engo.Run(opts, &play.PlayScene{})
}
